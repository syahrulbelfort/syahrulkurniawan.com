import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Navbar from './components/Navbar/Navbars';
import Home from './page/Beranda';
import ScrollToTop from './components/scroll to top/Scroll';
import Projects from './page/Projects';
import Blogs from './page/Blogs';
import About from './page/About';
import Footer from './components/footer/footer';
function App() {
  return (
    <Router>
      <Navbar />
      <ScrollToTop />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/projects' element={<Projects />} />
        <Route path='/blogs' element={<Blogs />} />
        <Route path='/about' element={<About />} />
      </Routes>
      <Footer />
    </Router>
  );
}

export default App;
