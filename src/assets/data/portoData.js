import BMIIndex from '../images/BMIIndex/BMI Index.png';
import SimonGame from '../images/SimonGame/Simon Game.png';
import Tindog from '../images/Tindog/Tindog.png';
import DrumKit from '../images/Drum Website/Drum Kit.png';
import DashBoard from '../images/Dashboard CRUD/dashboard.png'
const portoData = [
  {
    id: 1,
    image: 'ecommerce.png',
    title: 'phone e-commerce',
    shortDesc: 'Phone ecommerce is the online buying and selling of mobile phones.',
    longDesc: 'Phone ecommerce is the online buying and selling of mobile phones, offering customers a convenient and accessible way to browse a wide range of options and make purchases from anywhere.',
    techStacks: ['React', 'CSS'],
    link: {
      liveWeb: 'https://thriving-crepe-685373.netlify.app/',
      code: 'https://gitlab.com/syahrulbelfort/apple-store'
    }
  },
  {
    id: 2,
    image: 'netrailer.png',
    title: 'Netrailer ',
    shortDesc: 'Discover endless entertainment with our favorites website. ',
    longDesc: 'Discover endless entertainment with our streaming website. Watch all your favorite movies, TV shows, and original content on any device, anywhere. Start streaming now and enjoy the best in entertainment.',
    techStacks: ['React', 'Bootstrap'],
    link: {
      liveWeb: 'https://cosmic-biscochitos-af9607.netlify.app/',
      code: 'https://gitlab.com/syahrulbelfort/netrailer'
    }
  },
  {
    id: 3,
    image: DashBoard,
    title: 'Product dashboard ',
    shortDesc: 'Build dashboard product & implement CRUD process & auth using JWT',
    longDesc: 'Discover endless entertainment with our streaming website. Watch all your favorite movies, TV shows, and original content on any device, anywhere. Start streaming now and enjoy the best in entertainment.',
    techStacks: ['React', 'Tailwind'],
    link: {
      liveWeb: 'https://nutech-react-crud.vercel.app/',
      code: 'https://gitlab.com/syahrulbelfort/nutech-react-crud'
    }
  },
  {
    id: 4,
    image: 'game.png',
    title: 'Rock Paper Scissors',
    shortDesc: 'Rock Paper Scissors is a classic game that has been enjoyed by people.',
    longDesc: 'Rock Paper Scissors is a classic game that has been enjoyed by people of all ages for many years. The game is simple, yet exciting and challenging, and can be played by two players or against a computer.',
    techStacks: ['React', 'Bootstrap'],
    link: {
      liveWeb: 'https://aesthetic-douhua-f338c5.netlify.app/',
      code: 'https://gitlab.com/syahrulbelfort/binar-chapter-3/tree/develop'
    }
  },
  {
    id: 5,
    image: BMIIndex,
    title: 'BMI Index',
    shortDesc: 'BMI Index is a website to check your healthy body.',
    longDesc: 'BMI Index is a website to check your healthy body. If your body is healthy, you\'re good, but if it\'s not, the website you\'ll need this website',
    techStacks: ['React', 'Tailwind'],
    link: {
      liveWeb: 'https://bmi-web-index.vercel.app/',
      code: 'https://gitlab.com/syahrulbelfort/bmi-web-index'
    }
  },
  {
    id: 6,
    image: SimonGame,
    title: 'Simon Game',
    shortDesc: 'Simon is an electronic game that tests short-term memory skills.',
    techStacks: ['HTML', 'CSS', 'Javascript'],
    link: {
      liveWeb: 'https://nimble-axolotl-ba7edb.netlify.app/',
      code: 'https://gitlab.com/syahrulbelfort/simon-games-bos-challenge-udemy'
    }
  },
  {
    id: 7,
    image: Tindog,
    title: 'Tindog',
    shortDesc: 'a landing page about Tindog, which is like Tinder but for dogs.',
    techStacks: ['HTML', 'Bootstrap'],
    link: {
      liveWeb: 'https://animated-gingersnap-8ad704.netlify.app/',
      code: 'https://gitlab.com/syahrulbelfort/tindog-udemy'
    }
  },
  {
    id: 8,
    image: DrumKit,
    title: 'Drum Kit',
    shortDesc: 'Can\'t afford drums? but still want to play drums? Visit us.',
    techStacks: ['HTML', 'CSS', 'JavaScript'],
    link: {
      liveWeb: 'https://joyful-gumdrop-16c4bf.netlify.app',
      code: 'https://gitlab.com/syahrulbelfort/drum-kit-website'
    }
  }
];

export default portoData;
