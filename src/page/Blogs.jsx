import React, { useEffect, useState } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import AOS from '../utils/aos';
import { mediumFullData } from '../utils/axios';

export default function Blogs() {
  const [datas, setDatas] = useState();

  useEffect(() => {
    const fetchData = async () => {
      const response = await mediumFullData();
      const dataWithImages = response.map((blog) => {
        const parser = new DOMParser();
        const doc = parser.parseFromString(blog.content, 'text/html');
        const imgElement = doc.querySelector('img');
        const imageUrl = imgElement ? imgElement.src : '';
        return { ...blog, imageUrl };
      });
      setDatas(dataWithImages);
    };
    fetchData();
  }, []);

  useEffect(() => {
    AOS.refresh();
  }, []); // Add an empty dependency array to run the effect only once

  return (
    <>
      <div
        className='container containerProject containerProjectBlog'
        data-aos='fade-up'
      >
        <div className='box mt-5'>
          <h2 className='fw-bolder fs-1 mobileTextBlogs'>Blogs</h2>
        </div>
        <div className='projects mt-2 '>
          <Row className='gap-3 mt-4 '>
            {datas?.map((data) => {
              return (
                <Col className='col-lg-3 col-md-4' key={data.id}>
                  <Card style={{ width: '21rem' }} className='card-hover'>
                    <Card.Body>
                      <Card.Title className='fw-bolder mb-3 text-capitalize'>
                        {data.title}
                      </Card.Title>
                      <div className='screenProject'>
                        <Card.Img
                          variant='top'
                          className='ecommerce'
                          src={data.imageUrl}
                        />
                      </div>
                      <Card.Text className='mt-3 fs-6'>{data.title}</Card.Text>
                      <h3 className='projectLink'>
                        <a
                          href={data.link}
                          target='_blank'
                          rel='noreferrer'
                          className=' fs-4  text-dark

'
                        >
                          read more →
                        </a>
                      </h3>
                    </Card.Body>
                  </Card>
                </Col>
              );
            })}
          </Row>
        </div>
      </div>
    </>
  );
}
