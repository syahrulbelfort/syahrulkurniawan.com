import React, { useEffect } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import portoData from '../assets/data/portoData';
import AOS from '../utils/aos'

export default function Projects() {
  useEffect(() => {
    AOS.refresh();
  }, []); // Add an empty dependency array to run the effect only once

  return (
    <>
      <div className="container containerProject containerProjectMobile" data-aos="fade-up">
          <div className="box boxProject mt-5">
          <h2 className="fw-bolder fs-1 mobileTextProject">Projects</h2>
          </div>
        <div className="projects mt-2 " >
          <Row className="gap-3 mt-4 ">
            {portoData.map((data) => {
              return (
                <Col className="col-lg-3 col-md-4" key={data.id}>
                  <Card style={{ width: '21rem' }} className='card-hover'>
                    <Card.Body>
                      <Card.Title className="fw-bolder mb-3 text-capitalize">{data.title}</Card.Title>
                      <Card.Text className="mt-2 fs-6">{data.shortDesc}</Card.Text>
                      <div className="screenProject">
                        <Card.Img variant="top" className="ecommerce" src={data.image} />
                      </div>
                      <h3 className='projectLink mt-2'>
                        <a href={data.link.liveWeb} target="_blank" rel="noreferrer" className=" fs-5  text-dark

" >
                          live demo →
                        </a>
                      </h3>
                    </Card.Body>
                  </Card>
                </Col>
              );
            })}
          </Row>
        </div>
      </div>

    </>
  );
}
