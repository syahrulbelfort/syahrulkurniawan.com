import Banner from '../components/Home/banner/section1';
import About from '../components/Home/banner/ShortAbout';
import Portfolio1 from '../components/Home/banner/portfolio1';
import Portfolio2 from '../components/Home/banner/portfolio2';
import Portfolio3 from '../components/Home/banner/portofolio3';
import Contact from '../components/Home/banner/contact';
import Medium from '../components/Home/banner/medium';

export default function Beranda() {
  return (
    <>
      <Banner />
      <About />
      <Portfolio1 />
      <Portfolio2 />
      <Portfolio3 />
      <Medium />
      <Contact />
    </>
  );
}
