import React, { useEffect } from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import AOS from '../utils/aos';

export default function About() {
  useEffect(() => {
    AOS.refresh();
  }, []);

  return (
    <div className='cards containerAbout' id='about'>
      <Card>
        <Card.Body data-aos='fade-up'>
          <Row className='container--about containerTextAbout containerImageAbout'>
            <Col className='col-lg-6 col-sm-1'>
              <img
                alt='syahrul'
                className='about-image card-hover'
                src='me.png'
              />
            </Col>
            <Col>
              <div className='box'>
                <h5 className='fs-1 fw-bold mb-4'>About</h5>
              </div>
              <h2 className='h2--about mb-4'>Syahrul Kurniawan</h2>
              <p>
                While my professional journey has been rooted in marketing, my
                true passion lies in the world of programming. I have always
                been fascinated by the power of coding to create transformative
                solutions, streamline processes, and bring innovative ideas to
                life.
                <br />
                <br />
                Over the years, I have been diligently working to acquire the
                necessary skills and knowledge to transition my career into
                programming..
                <br />
                <br />
                I have invested countless hours in self-study, online courses,
                and coding boot camps to gain proficiency in programming
                languages such as JavaScript, and HTML/CSS. .<br />
                <br />
                Through personal projects and collaborations with fellow
                developers, I have developed practical experience and honed my
                problem-solving abilities. I am confident in my ability to
                contribute effectively to your programming team.
                <br />
                <br />{' '}
                <a
                  href='https://www.linkedin.com/in/syahrul-kurniawan-1717a5193/'
                  className='about-resume text-dark fw-bold'
                  target='_blank'
                  rel='noreferrer'
                >
                  {' '}
                  Connect with me
                </a>
              </p>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </div>
  );
}
