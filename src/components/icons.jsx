import React from 'react';

const picts = [
  { img: '/html.svg' },
  { img: '/css.svg' },
  { img: '/tailwind.png' },
  { img: '/javascript.svg' },
  { img: 'react.svg' },
  { img : 'redux.svg'}
];

export default function Icons() {
  return (
    <>
      {picts.map((pict, index) => (
        <div className="icon--wrapper icon-hover ">
            <img className='--icons' key={index} src={pict.img} alt={picts.img} />
        </div>
      ))}
    </>
  );
}
