import React from 'react';
import { RiGitlabFill } from 'react-icons/ri';
import { BsLinkedin } from 'react-icons/bs';
import { Link } from 'react-router-dom';
export default function footer() {
  return (
    <div className='footer'>
      <div className='footer-wrapper '>
        <h5>Copyright © 2023. All rights are reserved</h5>
        <div className='footer-icon'>
          <Link
            to={`https://gitlab.com/syahrulbelfort`}
            target='_blank'
            rel='noreferrer'
          >
            <RiGitlabFill size='25px' color='black' className='me-4' />
          </Link>
          <Link
            to={`https://www.linkedin.com/in/syahrul-kurniawan-1717a5193/`}
            target='_blank'
            rel='noreferrer'
          >
            <BsLinkedin size='25px' color='black' />
          </Link>
        </div>
      </div>
    </div>
  );
}
