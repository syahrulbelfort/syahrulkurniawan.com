import React, { useEffect } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { RiGitlabFill } from 'react-icons/ri';
import { Link } from 'react-router-dom';
import { MdOutlineLiveTv } from 'react-icons/md';
import AOS from '../../../utils/aos';

export default function Portofolio2() {
  useEffect(() => {
    AOS.refresh();
  }, []);

  return (
    <div className='container mt-5 ' data-aos='fade-up'>
      <Card className='cards-portfolio d-flex align-items-center'>
        <Card.Body>
          <Row className='port-reverse'>
            <Col>
              <div className='screen '>
                <img alt='netlaier' src='netrailer.png' />
              </div>
            </Col>
            <Col className=' text-center'>
              <div className='port-desc '>
                <h5 className='h5--about text-dark'>NETRAILER 🖥️</h5>
                <p>
                  Discover endless entertainment with our streaming website.
                  Watch all your favorite movies, TV shows, and original content
                  on any device, anywhere. Start streaming now and enjoy the
                  best in entertainment.
                </p>
              </div>
              <div className='stack'>
                <div className='mini-card'>
                  <p>React</p>
                </div>
                <div className='mini-card'>
                  <p>Bootstrap</p>
                </div>
              </div>
              <div className='porto-icons'>
                <Link
                  to={`https://gitlab.com/syahrulbelfort/netrailer`}
                  target='_blank'
                  rel='noreferrer'
                  className='d-flex'
                  style={{ textDecoration: 'none' }}
                >
                  <h5 className='text-dark'>Code</h5>{' '}
                  <RiGitlabFill className='ms-2 text-dark' size='23px' />
                </Link>
                <Link
                  to={`https://cosmic-biscochitos-af9607.netlify.app/`}
                  target='_blank'
                  rel='noreferrer'
                  className='d-flex'
                  style={{ textDecoration: 'none' }}
                >
                  <h5 className='ms-5 text-dark'>Live Demo</h5>{' '}
                  <MdOutlineLiveTv className='ms-2 text-dark' size='23px' />
                </Link>
              </div>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </div>
  );
}
