import React, { useEffect } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { RiGitlabFill } from 'react-icons/ri';
import { Link } from 'react-router-dom';
import { MdOutlineLiveTv } from 'react-icons/md';
import AOS from '../../../utils/aos';

export default function Portofolio3() {
  useEffect(() => {
    AOS.refresh();
  }, []);

  return (
    <div className='container mt-5'>
      <Card className='cards-portfolio d-flex align-items-center'>
        <Card.Body>
          <Row>
            <Col>
              <div className='screen'>
                <img alt='game' src='game.png' />
              </div>
            </Col>
            <Col className=' text-center'>
              <div className='port-desc'>
                <h5 className='h5--about text-dark'>GAME WEBSITE 🕹️</h5>
                <p>
                  Rock Paper Scissors is a classic game that has been enjoyed by
                  people of all ages for many years. The game is simple, yet
                  exciting and challenging, and can be played by two vs
                  computers.
                </p>
              </div>
              <div className='stack'>
                <div className='mini-card'>
                  <p>HTML</p>
                </div>
                <div className='mini-card'>
                  <p>Bootstrap</p>
                </div>
              </div>
              <div className='porto-icons'>
                <Link
                  to={`https://gitlab.com/syahrulbelfort/binar-chapter-3/tree/develop`}
                  target='_blank'
                  rel='noreferrer'
                  className='d-flex'
                  style={{ textDecoration: 'none' }}
                >
                  <h5 className='text-dark'>Code</h5>{' '}
                  <RiGitlabFill className='ms-2 text-dark ' size='23px' />
                </Link>
                <Link
                  to={`https://aesthetic-douhua-f338c5.netlify.app/`}
                  target='_blank'
                  rel='noreferrer'
                  className='d-flex'
                  style={{ textDecoration: 'none' }}
                >
                  <h5 className='ms-5 text-dark'>Live Demo</h5>{' '}
                  <MdOutlineLiveTv className='ms-2 text-dark' size='23px' />
                </Link>
              </div>
            </Col>
          </Row>
        </Card.Body>
        <h5 className='readMore mt-5'>
          <a className='readMore' href='/projects' rel='noopener noreferrer'>
            see more
          </a>
        </h5>
      </Card>
    </div>
  );
}
