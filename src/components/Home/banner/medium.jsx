import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import PostCard from './post';
import { mediumData } from '../../../utils/axios';
import AOS from '../../../utils/aos';

export default function Medium() {
  const [datas, setDatas] = useState([]);
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    AOS.refresh();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      const response = await mediumData();
      const dataWithImages = response.map((blog) => {
        const parser = new DOMParser();
        const doc = parser.parseFromString(blog.content, 'text/html');
        const imgElement = doc.querySelector('img');
        const imageUrl = imgElement ? imgElement.src : '';
        return { ...blog, imageUrl };
      });
      setDatas(dataWithImages);
    };
    fetchData();
  }, []);

  useEffect(() => {
    const mediaQuery = window.matchMedia('(max-width: 600px)');

    const handleMediaQueryChange = (event) => {
      setIsMobile(event.matches);
    };

    setIsMobile(mediaQuery.matches);
    mediaQuery.addEventListener('change', handleMediaQueryChange);

    return () => {
      mediaQuery.removeEventListener('change', handleMediaQueryChange);
    };
  }, []);

  return (
    <div id='contact'>
      <Card className='contact-medium '>
        <Card.Body className='card-body-medium container '>
          <h5 className='h5--about m-auto text-dark'>ARTICLES</h5>
          <div className='box m-auto'>
            <h3 className='h3-contact mt-2 mb-5'>
              Latest Insights & Stories 📖
            </h3>
          </div>
          <div className='grid-container'>
            {isMobile
              ? datas[0] && (
                  <PostCard
                    key={datas[0].id}
                    thumbnail={datas[0].imageUrl}
                    title={datas[0].title}
                    desc={datas[0].title}
                    pubDate={datas[0].pubDate}
                    link={datas[0].link}
                    className={'grid-item'}
                    author={datas[0].author}
                  />
                )
              : datas.map((blog) => (
                  <PostCard
                    key={blog.id}
                    thumbnail={blog.imageUrl}
                    title={blog.title}
                    desc={blog.title}
                    pubDate={blog.pubDate}
                    link={blog.link}
                    className={'grid-item '}
                    author={blog.author}
                  />
                ))}
          </div>
        </Card.Body>
        <h5 className='readMore'>
          <a className='readMore' href='/blogs'>
            read more
          </a>
        </h5>
      </Card>
    </div>
  );
}
