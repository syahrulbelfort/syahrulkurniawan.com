import React, { useState } from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import { RiGitlabFill } from 'react-icons/ri';
import { BsLinkedin } from 'react-icons/bs';
import { Link } from 'react-router-dom';
import { FaYoutube } from 'react-icons/fa';
import { FaTiktok } from 'react-icons/fa';
import { FaInstagramSquare } from 'react-icons/fa';

import Icons from '../../icons';
import AOS from '../../../utils/aos';

export default function Banner() {
  useState(() => {
    AOS.refresh();
  }, []);

  return (
    <div className='mt-5 section-1'>
      <Container data-aos='fade-up'>
        <Row xs='1' md='2' className='custom-flex '>
          <Col>
            <Container className='mt-5'>
              <h1 className='h1--title'>Software Engineer</h1>
              <p className='p--desc mt-4'>
                Hi, you can call me <span className='fw-bold box'>Syahrul</span>
                . Now i mostly work with React Ecosystem for Frontend
                Development
              </p>
              <div className='mt-5 mobile-icons'>
                <Link
                  className='me-4  '
                  to='https://www.linkedin.com/in/syahrul-kurniawan-1717a5193/'
                  target='_blank'
                  rel='noreferrer'
                >
                  <BsLinkedin
                    size='25px'
                    color='black'
                    className='icon-hover'
                  />
                </Link>
                <Link
                  to='https://gitlab.com/syahrulbelfort'
                  target='_blank'
                  rel='noreferrer'
                >
                  <RiGitlabFill
                    size='25px'
                    color='black'
                    className='icon-hover'
                  />
                </Link>
                <Link
                  to='https://www.youtube.com/@syahrulkrn/'
                  target='_blank'
                  rel='noreferrer'
                >
                  <FaYoutube
                    className='ms-4 icon-hover'
                    size='25px'
                    color='black'
                    alt='resume'
                  />
                </Link>
                <Link
                  to='https://www.tiktok.com/@syahrul_krn/'
                  target='_blank'
                  rel='noreferrer'
                >
                  <FaTiktok
                    className='ms-4 icon-hover'
                    size='25px'
                    color='black'
                    alt='resume'
                  />
                </Link>
                <Link
                  to='https://www.instagram.com/syahrulkrn/'
                  target='_blank'
                  rel='noreferrer'
                >
                  <FaInstagramSquare
                    className='ms-4 icon-hover'
                    size='25px'
                    color='black'
                    alt='resume'
                  />
                </Link>
              </div>
            </Container>
          </Col>
          <Col>
            <Container>
              <img
                alt='syahrul'
                src='me.png'
                className='card-hover rounded-circle sec1--image--margin'
                width='300px'
              />
            </Container>
          </Col>
        </Row>

        <div className='top-flex'>
          <div className='mt-5 container mobile--stack'>
            <h5>Tech Stack</h5>
            <div className='line'></div>
            <div className='icon--mobile'>
              <Icons />
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
}
