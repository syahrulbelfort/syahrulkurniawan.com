import React, { useEffect } from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import AOS from '../../../utils/aos';

export default function ShortAbout() {

  useEffect(()=>{
    AOS.refresh()
  },[])
  
  return (
    <div className='cards  ' id='about' data-aos="fade-up">
      <Card >
        <Card.Body>
          <Row className='container--about '>
            <Col className=''>
              <img alt='mac' className='card-hover' src='macdesk.png' width='420px' />
            </Col>
            <Col>
            <h5 className='h5--about mb-3 text-dark'>ABOUT</h5>
            <div className="box">
             <h2 className='h2--about mb-4'>Syahrul Kurniawan</h2>
            </div>
                <p className='fs-5 '>
                While my professional journey has been rooted in marketing, my true passion lies in the world of programming. I have always been fascinated by the power of coding to create transformative solutions, streamline processes, and bring innovative ideas to life. <a href='/about ' className='text-dark'>learn more about me</a> </p>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </div>
  );
}
