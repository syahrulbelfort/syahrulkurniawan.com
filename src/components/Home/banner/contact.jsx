import React from 'react'
import { Card } from 'react-bootstrap'
import {FaLocationArrow} from 'react-icons/fa'
import {AiTwotoneMail} from 'react-icons/ai'
import { Link } from 'react-router-dom'

export default function contact() {
  return (
    <div id='contact'> 
        <Card className='contact-card'>
            <Card.Body className='card-body-contact'>
                <h5 className='h5--about text-dark'>CONTACT</h5>
                <div className="box">                    
                <h3 className='h3-contact text-dark'>Don't be shy! Hit me up! 👇</h3>
                </div>
                <div className="contact-wrapper">
                <div className="contact-icon icon--wrapper">
                    <FaLocationArrow
                    size={`30px`}
                    className='icon-color'
                    />                  
                </div>
                <div className="contact-text">
                <h5 className='contact-text-wrapper'>Location</h5>
                <h5><Link className='text-dark' style={{textDecoration:'none'}}>Bekasi, Indonesia</Link></h5>
                </div>
                <div className="contact-icon icon--wrapper ">
                    <AiTwotoneMail
                    className='icon-color'
                    size={`30px`}
                    />                  
                </div>
                <div className="contact-text">
                <h5 className='contact-text-wrapper'>Email</h5>
                <h5><Link className='text-dark' to={`mailto:syahrulbelfort@gmail.com`} style={{textDecoration:'none'}}>syahrulbelfort@gmail.com</Link></h5>
                </div>
                </div>
                <div className="empty"></div>
            </Card.Body>
        </Card>
    </div>
  )
}
