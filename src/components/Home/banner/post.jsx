/* eslint-disable jsx-a11y/alt-text */
import React from 'react';

function PostCard(props) {
  return (
    <>
      <div className='blog-card'>
        <div className='blog-images'>
          <img className='blog-thumbnail' src={props.thumbnail} />
          <img className='blog-profile-image' src='me.png' />
        </div>
        <div className='blog-title-desc'>
          <h3>
            <a href={props.link} target='_blank' rel='noreferrer'>
              {props.title}
            </a>
          </h3>
          <p>{props.desc}</p>
        </div>
        <div className='author-date'>
          <p>{props.author}</p>
          <p>{props.pubDate}</p>
        </div>
      </div>
    </>
  );
}

export default PostCard;
