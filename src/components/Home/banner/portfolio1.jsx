import React, { useEffect } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { RiGitlabFill } from 'react-icons/ri';
import { Link } from 'react-router-dom';
import { MdOutlineLiveTv } from 'react-icons/md';
import portoData from '../../../assets/data/portoData';
import AOS from '../../../utils/aos';

export default function Portofolio() {
  useEffect(() => {
    AOS.refresh();
  }, []);

  return (
    <div className='container' data-aos='fade-up' id='portfolio'>
      <div className=' container title'>
        <h5 className='h5--about m-auto text-dark'>PORTFOLIO</h5>
        <div className='box m-auto'>
          <h3 className='h3-about m-auto'>Featured portoflio 🧩</h3>
        </div>
      </div>
      <Card className='cards-portfolio d-flex align-items-center'>
        <Card.Body>
          <Row>
            <Col>
              <div className='screen'>
                <img alt='ecommerce' src='ecommerce.png' />
              </div>
            </Col>
            <Col className=' text-center'>
              <div className='port-desc'>
                <h5 className='h5--about text-dark'>{portoData[0].title}</h5>
                <p>
                  Phone ecommerce is the online buying and selling of mobile
                  phones, offering customers a convenient and accessible way to
                  browse a wide range of options and make purchases from
                  anywhere.
                </p>
              </div>
              <div className='stack'>
                <div className='mini-card'>
                  <p>React</p>
                </div>
                <div className='mini-card'>
                  <p>CSS</p>
                </div>
              </div>
              <div className='porto-icons'>
                <Link
                  to='https://gitlab.com/syahrulbelfort/apple-store'
                  target='_blank'
                  rel='noreferrer'
                  className='d-flex'
                  style={{ textDecoration: 'none' }}
                >
                  <h5 className='text-dark'>Code</h5>{' '}
                  <RiGitlabFill className='ms-2 text-dark' size='23px' />
                </Link>
                <Link
                  to='https://thriving-crepe-685373.netlify.app/'
                  target='_blank'
                  rel='noreferrer'
                  className='d-flex'
                  style={{ textDecoration: 'none' }}
                >
                  <h5 className='ms-5 text-dark'>Live Demo</h5>{' '}
                  <MdOutlineLiveTv className='ms-2 text-dark' size='23px' />
                </Link>
              </div>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </div>
  );
}
