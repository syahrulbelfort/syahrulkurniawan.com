import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from 'react-router-dom';

function Navbars() {
  return (
    <Navbar className='nav' bg='white'>
      <div className='nav-custom-bg container  nav-max-w'>
        <Nav className='me-auto navMobile '>
          <Link to='/' className='nav-link'>
            Home
          </Link>
          <Link to='/projects' className='nav-link'>
            Projects
          </Link>
          <Link to='/blogs' className='nav-link'>
            Blog
          </Link>
          <Link to='/about' className='nav-link'>
            About
          </Link>
        </Nav>
      </div>
    </Navbar>
  );
}

export default Navbars;
