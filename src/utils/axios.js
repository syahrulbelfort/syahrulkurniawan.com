import axios from "axios";

const mediumLink = 'https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fmedium.com%2Ffeed%2F%40syahrulkrn';

export const mediumData = async () => {
  try {
    const response = await axios.get(mediumLink);
    return  response.data.items.slice(0, 3);

  } catch (error) {
    console.error('Error fetching data from Medium:', error);
  }
};
export const mediumFullData = async () => {
  try {
    const response = await axios.get(mediumLink);
    return  response.data.items;

  } catch (error) {
    console.error('Error fetching data from Medium:', error);
  }
};
